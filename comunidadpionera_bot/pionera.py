import pandas as pd
from errbot import BotPlugin, botcmd


class ComunidadPionera(BotPlugin):
    """Asistente para usar el directorio comercial de la comunidad pionera"""

    @botcmd
    def busco(self, msg, args):
        """Busca una persona con la ocupación indicada en los argumentos"""
        # data_frame = pd.read_excel(self.config['directorio_comercial'], sheet_name='Sheet1')
        data_frame = pd.read_csv(self.config['directorio_comercial'])

        # args trae la ocupación buscada.
        resultado = data_frame.loc[data_frame['Ocupación'].str.contains(args), ['Nombre', 'Número']]
        return resultado.to_string(index=False)

    def get_configuration_template(self):
            return {'directorio_comercial': 'cambieme',
                    }
