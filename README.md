# Err-bot Comunidad Pionera

Bot de Telegram de la Comunidad Pionera.

Actualmente ayuda a buscar en el directorio de oficios, servicios y productos.


## Uso

Para hacer consultas sobre oficios u ocupaciones utilice el comando `/busco`

 ~~~
 /busco Pintore

 Nombre      Número
 Jose Guey  “21212121”

 /busco Chambere
 Nombre      Número
 Jose Guey  “21212121”
      Luis  “21212121”
 ~~~

En el directorio comercial de la comunidad pionera se utiliza *en la medida de lo posible* la ["Clasificación de Ocupaciones de Costa Rica" del INEC (2011)](https://www.inec.cr/sites/default/files/documentos/inec_institucional/metodologias/documentos_metodologicos/38_metodologia_ocupaciones_2011.pdf).



## Colaborar con código

El bot está implementado en Python3, usando Errbot con Telegram como *backend* y pandas para hacer las búsquedas en el directorio comercial. Este plugin no está contempla poder ser instalado desde este repositorio sino que se pretende seguirlo adaptando a las necesidades de la Comunidad Pionera.

En la carpeta `comunidadpionera_bot` está el código del plugin. Una vez instalada y configurada una instancia de errbot se debe configurar el parámentro `BOT_EXTRA_PLUGIN_DIR` para que apunte a la carpeta `comunidadpionera_bot`.


Se recomienda utilizar un entorno virtual de Python3. Tres pasos básicos sobre entornos virtuales:
 * Crearlo: `$ python3 -m venv .venv`
 * Activarlo `$ source .venv/bin/activate`
 * Desactivarlo `(.venv) $ deactivate`
